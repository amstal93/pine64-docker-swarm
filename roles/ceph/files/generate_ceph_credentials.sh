#!/usr/bin/env bash

VIRTUAL_IP=$1
CEPH_IMAGE=$2

TMP_DIR=${HOME}/ceph_tmp
TMP_ETC_DIR=${TMP_DIR}/etc
TMP_VAR_DIR=${TMP_DIR}/var

OUTPUT_DIR=/tmp/ceph_stack

echo "-- clean ${TMP_DIR}"
sudo rm -Rf ${TMP_DIR}

echo "-- clean docker image"
docker kill tmp_ceph_mon
docker rm tmp_ceph_mon

echo "-- generate ceph conf"
docker run -d --net=host \
  --name tmp_ceph_mon \
  -v ${TMP_ETC_DIR}:/etc/ceph \
  -v ${TMP_VAR_DIR}:/var/lib/ceph \
  -e NETWORK_AUTO_DETECT=4 \
  ${CEPH_IMAGE} mon

# when keepalived is activated, /etc/ceph/ceph.conf is no more valid :(
docker exec -it tmp_ceph_mon sed -i "s/$VIRTUAL_IP//" /etc/ceph/ceph.conf

docker exec -it tmp_ceph_mon ceph mon getmap -o /etc/ceph/ceph.monmap

docker stop tmp_ceph_mon

cp -f ${TMP_ETC_DIR}/ceph.conf ${OUTPUT_DIR}/
cp -f ${TMP_ETC_DIR}/ceph.monmap ${OUTPUT_DIR}/
cp -f ${TMP_ETC_DIR}/ceph.client.admin.keyring ${OUTPUT_DIR}/
cp -f ${TMP_ETC_DIR}/ceph.mon.keyring ${OUTPUT_DIR}/
cp -f ${TMP_VAR_DIR}/bootstrap-mds/ceph.keyring ${OUTPUT_DIR}/ceph.bootstrap-mds.keyring
cp -f ${TMP_VAR_DIR}/bootstrap-osd/ceph.keyring ${OUTPUT_DIR}/ceph.bootstrap-osd.keyring
cp -f ${TMP_VAR_DIR}/bootstrap-rbd/ceph.keyring ${OUTPUT_DIR}/ceph.bootstrap-rbd.keyring
cp -f ${TMP_VAR_DIR}/bootstrap-rgw/ceph.keyring ${OUTPUT_DIR}/ceph.bootstrap-rgw.keyring

chown -R 1000:1000 ${OUTPUT_DIR}

sudo rm -Rf ${TMP_DIR}
